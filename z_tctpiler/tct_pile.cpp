#include <string>
#include <iostream>
#include <fstream>
#include <regex>

/// Input Parser by @author iain
// sourced: https://stackoverflow.com/a/868894
// Thank you for letting me be lazy, amen.
class InputParser{
    public:
        InputParser (int &argc, char **argv){
            for (int i=1; i < argc; ++i)
                this->tokens.push_back(std::string(argv[i]));
        }
        /// @author iain
        const std::string& getCmdOption(const std::string &option) const{
            std::vector<std::string>::const_iterator itr;
            itr =  std::find(this->tokens.begin(), this->tokens.end(), option);
            if (itr != this->tokens.end() && ++itr != this->tokens.end()){
                return *itr;
            }
            static const std::string empty_string("");
            return empty_string;
        }
        /// @author iain
        bool cmdOptionExists(const std::string &option) const{
            return std::find(this->tokens.begin(), this->tokens.end(), option) != this->tokens.end();
        }
    private:
        std::vector <std::string> tokens;
};

class WindowManager {
	
};

// filePaths & argNames are interrelated arrays where filePaths[x] locates	/
// the file whose information is to be inserted into argNames[x].			/
// Not all argNames have extant filePaths.

const unsigned num = 32;

const std::string filePaths [] = {
	// Code 1
	"",
	"Code1/01_election.json",
	"Code1/02_candidate.json",
	"Code1/03_running_mate.json",
	"Code1/04_opponents_default.json",
	"",
	"",
	"",

	"",

	// Code 2
	"",
	"Code2/01_questions.json",
	"Code2/02_answers.json",
	"Code2/03_states.json",
	"Code2/04_issues.json",
	"Code2/05_state_issue_score.json",
	"Code2/06_candidate_issue_score.json",
	"Code2/07_running_mate_issue_score.json",
	"Code2/08_candidate_state_multiplier.json",
	"Code2/09_answer_score_global.json",
	"Code2/10_answer_score_issue.json",
	"Code2/11_answer_score_state.json",
	"Code2/12_answer_feedback.json",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"Code2/zz_MapData.js"
};

const std::string argNames [] = {
	"/* Code 1 */",
	"campaignTrail_temp.election_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.candidate_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.running_mate_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.opponents_default_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.show_premium = ;",
	"campaignTrail_temp.premier_ab_test_version = ;",
	"campaignTrail_temp.credits = \"\";",

	"",

	"/* Code 2 */",
	"campaignTrail_temp.questions_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.answers_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.states_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.issues_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.state_issue_score_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.candidate_issue_score_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.running_mate_issue_score_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.candidate_state_multiplier_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.answer_score_global_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.answer_score_issue_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.answer_score_state_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.answer_feedback_json = JSON.parse(\"$$\");",
	"campaignTrail_temp.candidate_image_url = \"\";",
	"campaignTrail_temp.running_mate_image_url = \"\";",
	"campaignTrail_temp.candidate_last_name = \"\";",
	"campaignTrail_temp.running_mate_last_name = \"\";",
	"campaignTrail_temp.running_mate_state_id = \"\";",
	"campaignTrail_temp.player_answers = [];",
	"campaignTrail_temp.player_visits = [];",
	"campaignTrail_temp.answer_feedback_flg = ;",
	"campaignTrail_temp.game_start_logging_id = \"\";",
	"\n/* Map Data */\n$$"
};

// tctpile by Calph (and or others)
// -h, --help, --usage		displays this message. Will not execute.
// -o=<string>				sets the output filename.
// -f, --force				program will not errorcheck or enforce protections.
const char* HELPTEXT = "tctpile by Calph (and or others)\n-h, --help, --usage		displays this message, program will not proceed.\n-o=<string>			sets the output filename.\n-f, --force			program will not errorcheck or enforce protections.";

void tctpile(int numArgs, std::string outFile="test.txt") {
	std::ofstream ofs (outFile);
	
	for(int i = 0; i < num; i++) {
		std::string out = argNames[i]; 

		if (std::ifstream ifs {filePaths[i]}) {
			//Read to in
			std::string in((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());

			//Join Lines (Shorten Space)
			in = std::regex_replace(in, std::regex("( |\t|\n)+"), "");

			//Escape Quotes
			in = std::regex_replace(in, std::regex("\""), "\\\"");

			//Insert
			out = std::regex_replace(out, std::regex("\\$\\$"), in);

			ifs.close();
		}

		// Output
		out += "\n";
		ofs << out;
	}

	ofs.close();
}

int main (int argc, char** argv) {
	InputParser input(argc, argv);

	// Help/Usage
	if ( input.cmdOptionExists("-h") || input.cmdOptionExists("--help") || input.cmdOptionExists("--usage")) {
		std::cout << HELPTEXT << std::endl;
		return 0;
	}

	// Force
	if ( input.cmdOptionExists("-f") || input.cmdOptionExists("--force")){

	}

	// Output Filename
	const std::string &outFile = input.getCmdOption("-o");

	// Run
	if (!outFile.empty())
		tctpile(num, outFile);
	else
		tctpile(num);
}